/*
 - CommentBox
     - CommentList
        - Comment
    - CommentForm
 */

var React       = require("react"),
    Request     = require("superagent"),
    CommentList = require("./CommentList.jsx"),
    CommentForm = require("./CommentForm.jsx");

var CommentBox = React.createClass({

    loadCommentsFromServer: function(){
        Request.get(this.props.url).end((function(res){
            if (res.ok) {
                this.setState({ data: res.body });
            } else {
                console.error(this.props.url, res.status, res.error.toString());
            }
        }).bind(this))
    },

    handleCommentSubmit: function (comment) {
        console.log(comment);
    },

    getInitialState: function(){
        return { data: [] };
    },

    componentDidMount: function (){
        this.loadCommentsFromServer();
        setInterval(this.loadCommentsFromServer, this.props.pollInterval);
    },

    render: function(){
        return (
            <div className="commentBox">
                Hello world! I am a CommentBox
                <CommentList data={this.state.data} />
                <CommentForm onCommentSubmit={this.handleCommentSubmit} />
            </div>
        );
    }
});

// component initialisation
window.addEventListener("load", function(){
    React.render(
        <CommentBox url="comments.json" pollInterval={2000} />,
        document.getElementById("content")
    );
});
var React    = require("react"),
    Showdown = require("showdown");

module.exports = React.createClass({

    displayName: "Comment",

    render: function () {
        var converter = new Showdown.converter(),
            rawMarkup = converter.makeHtml(this.props.children.toString());

        return (
            <div className="comment">
                <h2 className="commentAuthor">
                    {this.props.author}
                </h2>
                <span dangerouslySetInnerHTML={{__html: rawMarkup}} />
            </div>
        );
    }
});
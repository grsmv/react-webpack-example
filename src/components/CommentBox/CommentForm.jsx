var React = require("react");

module.exports = React.createClass({

    displayName: "CommentForm",

    handleSubmit: function(e){
        e.preventDefault();
        var author = this.refs.author.getDOMNode().value.trim(),
            text = this.refs.text.getDOMNode().value.trim();

        // clearing inputs
        this.props.onCommentSubmit({ author:author, text:text });
        this.refs.author.getDOMNode().value = "";
        this.refs.text.getDOMNode().value   = "";
    },

    render: function () {
        return (
            <form className="commentForm" onSubmit={this.handleSubmit}>
                <input type="text" placeholder="Name" ref="author" />
                <input type="text" placeholder="Say something..." ref="text" />
                <input type="submit" value="Post" />
            </form>
        );
    }
});
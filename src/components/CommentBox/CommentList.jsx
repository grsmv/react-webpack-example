var React   = require("react"),
    Comment = require("./Comment.jsx");

module.exports = React.createClass({

    displayName: "CommentList",

    render: function () {
        var commentNodes = this.props.data.map(function(comment){
            return (
                <Comment author={comment.author}>
                {comment.text}
                </Comment>
            );
        });

        return (
            <div className="commentList">
              {commentNodes}
            </div>
        );
    }
});
var webpack = require("webpack");

module.exports = {
    entry: "./src/main",
    output: {
        path: __dirname,
        filename: "build/bundle.js",
        sourceMapFilename: "[file].map"
    },
    module: {
        loaders: [
            { test: /\.css/,  loader: "style-loader!css-loader" },
            { test: /\.jsx$/, loader: "jsx-loader?harmony" }
        ]
    },
    plugins: [
        new webpack.PrefetchPlugin("react"),
        new webpack.PrefetchPlugin("showdown"),
        new webpack.PrefetchPlugin("superagent"),
        // new webpack.optimize.UglifyJsPlugin()
    ]
};